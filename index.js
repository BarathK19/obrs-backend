var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var cors = require('cors')
app.use(cors())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

var dataMockJson = [
    {
        name:"Software Systems",
        image:"/"
    },
    {
        name:"Engg Systems",
        image:"/"
    },
    {
        name:"Computer Systems",
        image:"/"
    },
    {
        name:"Computer Systems",
        image:"/"
    }
]


app.get('/', function(req, res){
   res.send(dataMockJson);
});


app.post('/login', (req, res) => {
  if(req.body.uname == "alien" && req.body.pwd == "password"){
      res.status(200);
      res.send("success");
  }
  else{
      res.status(401);
      res.send("vellila podaa");
  }
})

app.listen(3000);